package za.co.ilab.automation.page;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import za.co.ilab.automation.utilities.Utilities;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class SouthAfricaPage {

    public static final Logger log4J = Logger.getLogger(SouthAfricaPage.class.getName());
    private WebDriver driver;
    private ExtentTest extentTest;

    public SouthAfricaPage(WebDriver driver, ExtentTest extentTest) throws IOException {
        log4J.info("==================================== South Africa Careers screen ===================================== ");
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.extentTest = extentTest;
        this.extentTest.info("Navigating to South Africa careers screen", MediaEntityBuilder.createScreenCaptureFromPath( Utilities.getScreenShot(this.driver)).build());
    }

    public void clickOnOpenVacancy() throws IOException {
        List<WebElement> elementsList =  driver.findElements(By.className("wpjb-category-south-africa"));

        elementsList.get(0).click();

        this.extentTest.info("Clicking a specialist position", MediaEntityBuilder.createScreenCaptureFromPath( Utilities.getScreenShot(this.driver)).build());
    }
}
